import type { Config } from "tailwindcss";
import { withOptions } from 'tailwindcss/plugin'

const config: Config = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      label: {
        color: 'rgb(255 255 255 / 33%)'
      },
      backgroundImage: {
        "gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
        "gradient-conic":
          "conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
      },
    },
  },
  plugins: [
    withOptions(() => function ({ addBase, addComponents, addUtilities, theme }) {
      addBase({
        h3: {
          fontSize: '24px',
          fontWeight: theme('fontWeight.bold'),
        },
        h4: {
          fontSize: '20px',
          fontWeight: theme('fontWeight.bold'),
        },
        'input::placeholder': {
          color: 'rgb(255 255 255 / 33%)',
        }
      })

      addComponents({
        '.form-label': {
          color: 'rgb(255 255 255 / 33%)',
          fontWeight: theme('fontWeight.medium'),
          fontSize: '13px',
        },
      })
    })
  ],
};
export default config;
