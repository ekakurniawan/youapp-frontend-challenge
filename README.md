# Youapp - Frontend Technical Challenge

## Preview
<img src="https://gitlab.com/ekakurniawan/youapp-frontend-challenge/-/raw/main/public/static/images/collage.png" width="860">

## Tasks

- [x] Create Mobile Webapp using Next.js.
- [x] Create with Next.js 13 with app router based on design.
- [x] Implement custom and modular Tailwind CSS configuration.
- [x] Usage of good design patterns & react architecture patterns.
- [x] Usage of good practices in react.
- [x] Build and Connect with API.

## Specifications

- [x] Without UI libraries
- [x] Next.js @13
- [x] Tailwind CSS @3.3.0
- [x] React @18.2
- [x] Typescript @5.3.3
- [x] Next-PWA @5.6.0

## There is something missing from this project

- Where is the API located for uploading images?
  > **DONE**, using proxy to handle image upload
- In the mockup there is `gender`, but in the API there is none.
  > **PENDING**, uses a proxy so that it is stored in server memory
- What are the units of `units`? on the mockup you can change `cm` or `inches`, but in the API there is only `number`.
  > **DONE**, default using `cm` and any value will be converted to `cm`.
- On the profile page mockup, there is a `3 dot button`, what is its function?
  > **DONE**, will show confirmation to reset Profile.
- Where do you get the icons for `horoscope` and `zodiac`?
  > **DONE**, using svg from from server [youapp.ai](https://youapp.ai)
- Where the icon is `visible`, in the mockup there is none.
  > **DONE**, modify the invisible icon.

## This project can be improved further by / Improvement

- [x] Proxy API _(This creates a delay, but this is necessary because the available APIs need to be changed, so I improved the data format so that the application functionality is better, like uploading image and usage cookies for auth)_.
- [x] Separated input by type to reduce unused code.
- [x] Dynamic component.
- [x] Using internal API to handle astrology [source](src/app/api/astrology/route.ts).
- [x] Using Cookies for auth.
- [x] Progressive Web App (PWA).
- [x] Using Placeholder/Singleton.
- [x] Add Text Shadow to text with possibility of having a white background.
- [x] Add icon for `horoscope` and `zodiac` from [youapp.ai](https://youapp.ai).
- [x] Add icon for `visible`, modify `invisible` icon.
- [x] Make 3 dot button on profile page, onClick will show confirmation to reset Profile.
- [x] Add loading on button when on process.
- [x] hide scrollbar because it's not needed.
- [x] Using API to upload images, but only stored on server memory.
- [ ] Using input formatting rules & validation.



