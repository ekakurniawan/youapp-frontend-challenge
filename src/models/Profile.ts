export type Profile = {
  email: string,
  username: string,
  interests: string[],
  // 
  name?: string,
  birthday?: string,
  height?: number,
  weight?: number,
  // 
  gender?: string,
  image?: string,
}