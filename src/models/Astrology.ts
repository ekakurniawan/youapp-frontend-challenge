export type Astrology = {
  symbol: string;
  name: string;
  abbr: string;
  range: [[number, number], [number, number]];
};