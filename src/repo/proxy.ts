

function $fetch(method: string, path: string, options: {
  query?: string | string[][] | Record<string, string> | URLSearchParams | undefined;
  json?: Record<string, any>;
  form?: FormData;
  encoded?: Record<string, any>;
} = {}) {

  const query = options.query && new URLSearchParams(options.query).toString();

  return fetch(`/api${path}${query ? `?${query}` : ''}`, {
    method,
    ...(method === 'GET' ? {} : {
      body: options.json
        ? JSON.stringify(options.json)
        : options.encoded
          ? new URLSearchParams(query)
          : options.form ?? '',
    }),
  })
    .then(res => res.json())
}

async function register(email: string, username: string, password: string) {
  return $fetch('POST', '/register', {
    json: {
      email,
      username,
      password
    }
  })
  //201
}

async function login(identify: string, password: string) {
  return $fetch('POST', '/login', {
    json: {
      identify,
      password
    }
  })
  //200
}

async function getProfile() {
  return $fetch('GET', '/profile')
  //200, 401
}

async function createProfile(payload: {
  name: string,
  birthday: string,
  height: number,
  weight: number,
  interests: string[],
  gender?: string,
  image?: string
}) {
  const { image, interests, ...others } = payload
  const formData = new FormData();

  if (image) {
    const response = await fetch(image);
    const blob = await response.blob();
    formData.append('image', blob);
  }

  interests && formData.append('interests', interests.join(','));
  Object.entries(others).forEach(([key, value]) => {
    formData.append(key, value.toString())
  })
  return $fetch('POST', '/profile', {
    form: formData,
  })
  //201
}

async function updateProfile(payload: {
  name: string,
  birthday: string,
  height: number,
  weight: number,
  interests: string[]
  gender?: string,
  image?: string
}) {
  const { image, interests, ...others } = payload
  const formData = new FormData();

  if (image) {
    const response = await fetch(image);
    const blob = await response.blob();
    formData.append('image', blob);
  }

  interests && formData.append('interests', interests.join(','));
  Object.entries(others).forEach(([key, value]) => {
    formData.append(key, value.toString())
  })
  return $fetch('PUT', '/profile', {
    form: formData,
  })
  //200
}

function DataURIToBlob(dataURI: string) {
  const splitDataURI = dataURI.split(',')
  const byteString = splitDataURI[0].indexOf('base64') >= 0 ? atob(splitDataURI[1]) : decodeURI(splitDataURI[1])
  const mimeString = splitDataURI[0].split(':')[1].split(';')[0]

  const ia = new Uint8Array(byteString.length)
  for (let i = 0; i < byteString.length; i++)
    ia[i] = byteString.charCodeAt(i)

  return new Blob([ia], { type: mimeString })
}

export {
  register,
  login,
  getProfile,
  createProfile,
  updateProfile
}