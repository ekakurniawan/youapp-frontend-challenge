const BASE_URL = process.env.NEXT_PUBLIC_API;

function $fetch(path: string, options: any, token?: string): Promise<{
  data?: any,
  error?: any,
}> {
  return fetch(`${BASE_URL}${path}`, {
    headers: {
      'Content-Type': 'application/json',
      ...(token ? { 'x-access-token': token } : {})
    },
    ...options
  })
    .then(async (res) => {
      if (res.status === 500) {
        return {
          error: {
            code: 401,
            message: 'Unauthorized'
          }
        }
      } else {
        return {
          data: await res.json(),
        }
      }
    })
    .catch(err => ({
      error: {
        code: 500,
        message: err
      }
    }))
}

function register(email: string, username: string, password: string) {
  return $fetch('/register', {
    method: 'POST',
    body: JSON.stringify({ email, username, password })
  })
}

function login(email: string, username: string, password: string) {
  return $fetch('/login', {
    method: 'POST',
    body: JSON.stringify({ email, username, password })
  })
}

async function getProfile(token: string) {
  return await $fetch('/getProfile', {
    method: 'GET'
  }, token)
}

function createProfile(token: string, payload: {
  name: string,
  birthday: string,
  height: number,
  weight: number,
  interests: string[]
}) {
  return $fetch('/createProfile', {
    method: 'POST',
    body: JSON.stringify(payload)
  }, token)
}

function updateProfile(token: string, payload: {
  name: string,
  birthday: string,
  height: number,
  weight: number,
  interests: string[]
}) {
  return $fetch('/updateProfile', {
    method: 'PUT',
    body: JSON.stringify(payload)
  }, token)
}

export { register, login, getProfile, createProfile, updateProfile }