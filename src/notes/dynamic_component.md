## WITH SET

```jsx
<div>
  {
    fields.map((item, index) => {
            const sameFields = {
              value: payload[item.key],
              placeholder: item.placeholder,
              onInput: (newVal: unknown) => handleInput(item.key, newVal)
            }
            const componentSet: any = {
              'select': [SelectField, { options: item.options, }],
              'date': [DateField, {}],
              'unit': [UnitField, { units: item.units, }],
              'text': [TextField, { disabled: item.disabled, outlined: true, alignRight: true, }],
            }
            const [dynamicComponent, args] = componentSet[item.type ?? 'text'] || [() => <div />, {}]

            return (
              <div key={index} className="flex items-center justify-between -mr-2">
                <span className="form-label flex-1">
                  {item.label}
                </span>

                {dynamicComponent({ ...sameFields, ...args })}
              </div>
            )
  }
</div>
```

## SEPARATED LINE

```jsx
<div>
  {item.type === "select" && (
    <SelectField
      value={payload[item.key]}
      placeholder={item.placeholder}
      options={item.options}
      onInput={(newVal) => handleInput(item.key, newVal)}
    />
  )}
  {item.type === "date" && (
    <DateField
      value={payload[item.key]}
      placeholder={item.placeholder}
      onInput={(newVal) => handleInput(item.key, newVal)}
    />
  )}
  {item.type === "unit" && (
    <UnitField
      value={payload[item.key]}
      placeholder={item.placeholder}
      units={item.units}
      onInput={(newVal) => handleInput(item.key, newVal)}
    />
  )}
  {item.type === "text" && (
    <TextField
      value={payload[item.key]}
      placeholder={item.placeholder}
      disabled={item.disabled}
      outlined
      alignRight
      onInput={(newVal) => handleInput(item.key, newVal)}
    />
  )}
</div>
```

## TERNARY

```jsx
<div>
  {item.type === "select" ? (
    <SelectField
      value={payload[item.key]}
      placeholder={item.placeholder}
      options={item.options}
      onInput={(newVal) => handleInput(item.key, newVal)}
    />
  ) : item.type === "date" ? (
    <DateField
      value={payload[item.key]}
      placeholder={item.placeholder}
      onInput={(newVal) => handleInput(item.key, newVal)}
    />
  ) : item.type === "unit" ? (
    <UnitField
      value={payload[item.key]}
      placeholder={item.placeholder}
      units={item.units}
      onInput={(newVal) => handleInput(item.key, newVal)}
    />
  ) : (
    <TextField
      value={payload[item.key]}
      placeholder={item.placeholder}
      disabled={item.disabled}
      outlined
      alignRight
      onInput={(newVal) => handleInput(item.key, newVal)}
    />
  )}
</div>
```
