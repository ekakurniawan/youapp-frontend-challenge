'use client'
import { useRouter } from 'next/navigation'
import { AppBar, Scaffold } from '@/components/Panes';
import BackButton from "@/components/Common/BackButon";
import RainbowButton from '@/components/Common/RainbowButton';
import GoldenText from '@/components/Common/GoldenText';
import { PasswordField, TextField } from '@/components/Inputs';
import { useState } from 'react';
import { proxy } from '@/repo'

export default function LoginPage() {
  const router = useRouter()
  const appBar = (<AppBar leading={
    <BackButton onClick={() => router.back()} />
  } />)

  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  async function doLogin() {
    setIsLoading(true)
    const { code, data, message } = await proxy.login(email, password)
    setIsLoading(false)

    if (code === 200) {
      router.replace('/profile')
    } else {
      alert('Invalid credentials')
    }
  }

  const [isLoading, setIsLoading] = useState(false)

  const delay = (ms: number) => new Promise((resolve) => setTimeout(resolve, ms))

  return (
    <Scaffold appBar={appBar} background={1}>
      <div className="flex-1 flex flex-col justify-center px-[18px]">
        <h3 className="mb-[25px]">
          Login
        </h3>

        <div className='flex flex-col gap-4 mb-8'>
          <TextField
            value={email}
            placeholder='Enter Username/Email'
            large
            onInput={(newVal) => setEmail(newVal)}
          />

          <PasswordField
            value={password}
            placeholder='Enter Password'
            large
            onInput={(newVal) => setPassword(newVal)}
            onKeyDown={(e: React.KeyboardEvent<HTMLInputElement>) => {
              e.key === 'Enter' && doLogin()
            }}
          />
        </div>

        <RainbowButton
          disabled={isLoading || !email || !password}
          isLoading={isLoading}
          onClick={doLogin}>
          Login
        </RainbowButton>

        <p className="mt-[52px] flex items-center justify-center gap-1 text-[13px] font-medium">
          <span>No account?</span>
          <GoldenText className='cursor-pointer' onClick={() => router.replace('/register')}>
            Register here
          </GoldenText>
        </p>

      </div>
    </Scaffold>
  );
}
