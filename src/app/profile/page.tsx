'use client';
import { Scaffold, AppBar, Card } from "@/components/Panes";
import { IconEdit, IconHorizontalDot, IconLoading } from "@/components/Icons";

import {
  Editor as AboutEditor,
  Display as AboutDisplay,
  Banner as AboutBanner,
  BannerPlaceholder, AboutPlaceholder
} from "@/components/About";

import {
  Editor as InterestEditor,
  Display as InterestDisplay,
  InterestPlaceholder
} from "@/components/Interest";

import GoldenText from "@/components/Common/GoldenText";
import { useEffect, useMemo, useState } from "react";
import BackButton from "@/components/Common/BackButon";
import { useRouter } from 'next/navigation'
import { Profile } from "@/models";
import { proxy } from '@/repo'
import { Placeholder } from "@/components/Placeholder";

export default function ProfilePage() {
  const router = useRouter()
  const [isFetching, setIsFetching] = useState(true)
  const [about, setAbout] = useState<'empty' | 'edit' | 'display'>('empty')
  const [interest, setInterest] = useState<'empty' | 'edit' | 'display'>('empty')
  const [isSaving, setIsSaving] = useState(false)

  const [profile, setProfile] = useState<Profile>({
    email: '',
    username: '',
    interests: [],
    // 
    name: '',
    birthday: '',
    height: 0,
    weight: 0,
    // 
    gender: undefined,
    image: undefined
  })

  const hasAbout = useMemo(() => {
    const { name, birthday, height, weight } = profile;
    return !!name && !!birthday && !!height && !!weight;
  }, [profile]);

  const [temp, setTemp] = useState<Profile>({
    email: '',
    username: '',
    name: '',
    birthday: '',
    height: 0,
    weight: 0,
    interests: [],
  })

  useEffect(() => {
    doGetProfile()
    return () => { }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  async function doGetProfile() {
    setIsFetching(true)
    const { code, data, message } = await proxy.getProfile()

    if (code === 200) {
      const {
        email, username, interests,
        name, birthday, height, weight,
        gender, image,
      } = data

      setProfile(() => ({
        email, username, interests,
        name, birthday, height, weight,
        gender, image,
      }))

      setAbout((name || birthday || height || weight) ? 'display' : 'empty')
      setInterest(interests?.length > 0 ? 'display' : 'empty')
      setIsFetching(false)
    } else {
      router.replace('/')
    }
  }

  async function saveProfile(callback: () => void) {
    setIsSaving(true)
    const fn = hasAbout ? proxy.updateProfile : proxy.createProfile
    const {
      name = '', birthday = '', height = 0, weight = 0, // about
      interests = [], // interests
      gender = '1', image // missing
    } = temp

    fn({
      name,
      birthday,
      height,
      weight,
      interests,
      gender,
      image,
    }).then((response) => {
      doGetProfile().then(() => {
        setProfile((prev) => ({
          ...prev,
          ...(gender && ({ gender })),
          ...(image && { image })
        }))

        setIsSaving(false)
      });
      const { code, data, message } = response
      if (code === 200) {
        callback?.()
      } else {
        console.warn(message)
      }
    })
  }

  async function resetProfile() {
    const confirmed = await confirm('Are you sure you want to reset your profile?')
    if (confirmed) {
      proxy.updateProfile({
        name: '',
        birthday: '',
        height: 0,
        weight: 0,
        interests: [],
      }).then((response) => {
        doGetProfile();
      })
    }
  }

  const appBar = (<AppBar
    title={interest === 'edit' ? '' : (profile['username'] ? `@${profile['username']}` : '')}
    leading={(
      <BackButton onClick={() => {
        if (interest === 'edit') {
          profile.interests?.length > 0
            ? setInterest('display') :
            setInterest('empty')
        } else if (about === 'edit') {
          if (hasAbout) {
            setAbout('display')
          } else {
            setAbout('empty')
          }
        } else {
          router.back()
        }
      }} />
    )}
    trailing={interest === 'edit' ? (
      isSaving ? (
        <IconLoading />
      ) : (
        <span className='text-sm font-semibold cursor-pointer select-none' style={{
          background: 'linear-gradient(135deg, #ABFFFD 2.64%, #4599DB 102.4%, #AADAFF 102.4%)',
          backgroundClip: 'text',
          WebkitBackgroundClip: 'text',
          WebkitTextFillColor: 'transparent'
        }}
          onClick={() => {
            saveProfile(() => {
              // setInterest('display')
            })
          }}
        >
          Save
        </span>
      )
    ) : (
      <>
        <div className="w-6 h-6 flex items-center justify-center cursor-pointer select-none"
          onClick={() => {
            resetProfile()
          }}
        >
          <IconHorizontalDot />
        </div>
      </>
    )}
  />)

  return (
    <>
      <Scaffold appBar={appBar} background={interest === 'edit' ? 1 : 0}>
        <div className="flex-1 flex flex-col justify-start gap-6 px-2"
          style={{
            opacity: interest === 'edit' ? 0 : 1,
            transition: 'opacity 0.6s ease, height 0.6s ease',
          }}
        >
          <Placeholder
            ready={!isFetching}
            customPlaceholder={BannerPlaceholder}
            showLoadingAnimation
            delay={500}>
            <AboutBanner profile={profile} />
          </Placeholder>

          <Placeholder
            ready={!isFetching}
            customPlaceholder={AboutPlaceholder}
            showLoadingAnimation
            delay={500}>
            <Card>
              <div className="relative flex justify-between">
                <p className="text-sm font-bold">
                  About
                </p>

                {
                  ['empty', 'display'].includes(about) ? (
                    <button
                      aria-label="Edit"
                      className="-mr-[14px] cursor-pointer select-none"
                      onClick={() => {
                        setAbout('edit')
                      }}
                    >
                      <IconEdit />
                    </button>
                  ) : (
                    isSaving ? (
                      <IconLoading />
                    ) : (
                      <button
                        aria-label="Save & Update">
                        <GoldenText
                          className="text-xs cursor-pointer select-none"
                          onClick={() => {
                            saveProfile(() => { })
                          }}>
                          Save & Update
                        </GoldenText>
                      </button>
                    )
                  )
                }
              </div>

              {
                about === 'empty' ? (
                  <div className="my-2 text-sm text-white/[52%] font-medium">
                    Add in your about to help others know you better
                  </div>
                ) : (
                  about === 'edit' ? (
                    <AboutEditor
                      profile={profile}
                      onChange={(newVal: any) => setTemp(prev => ({ ...prev, ...newVal }))}
                    />
                  ) : (
                    <AboutDisplay profile={profile} />
                  )
                )
              }
            </Card>
          </Placeholder>

          <Placeholder
            ready={!isFetching}
            customPlaceholder={InterestPlaceholder}
            showLoadingAnimation
            delay={500}>
            <Card>
              <div className="relative flex justify-between">
                <p className="text-sm font-bold">
                  Interest
                </p>

                <button
                  aria-label="Edit"
                  className="-mr-[14px] cursor-pointer select-none"
                  onClick={() => {
                    setInterest('edit')
                  }}
                >
                  <IconEdit />
                </button>
              </div>

              {
                interest === 'empty' ? (
                  <div className="my-2 text-sm text-white/[52%] font-medium">
                    Add in your interest to find a better match
                  </div>
                ) : (
                  interest === 'edit' ?
                    null
                    : <InterestDisplay value={profile['interests']} />
                )
              }
            </Card>
          </Placeholder>

        </div>
      </Scaffold>


      <div
        style={{
          position: 'absolute',
          inset: 0,
          transition: 'opacity 0.6s ease, transform 0.6s ease',
          ...(interest === 'edit' ? {
            transform: 'translateY(0%)',
            opacity: 1,
          } : {
            transform: 'translateY(-100%)',
            opacity: 0,
            pointerEvents: 'none',
          }),
        }}
      >
        <Scaffold appBar={appBar} background={interest === 'edit' ? 1 : 0}>
          <div className="flex-1 pt-3 pb-6 flex flex-col px-6">
            <InterestEditor value={profile['interests']} onChange={newVal => {
              setTemp(prev => ({
                ...prev,
                interests: newVal,
              }))
            }} />
          </div>
        </Scaffold>
      </div>

    </>
  )
}
