'use client'
import { useRouter } from 'next/navigation'
import { AppBar, Scaffold } from '@/components/Panes';
import BackButton from "@/components/Common/BackButon";
import RainbowButton from '@/components/Common/RainbowButton';
import GoldenText from '@/components/Common/GoldenText';
import { PasswordField, TextField } from '@/components/Inputs';
import { useState } from 'react';
import { proxy } from '@/repo'

export default function RegisterPage() {
  const router = useRouter()
  const appBar = (<AppBar leading={
    <BackButton onClick={() => router.back()} />
  } />)

  const [email, setEmail] = useState('')
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const [confirmPassword, setConfirmPassword] = useState('')

  async function doRegister() {
    if (password !== confirmPassword) {
      alert('Password and Confirm Password must be the same')
      return
    }

    const { code, data, message } = await proxy.register(email, username, password)
    if (code === 200) {
      alert('User has been created successfully')
      router.replace('/login')
    } else {
      alert(message)
    }
  }

  return (
    <Scaffold appBar={appBar} background={1}>
      <div className="flex-1 flex flex-col justify-center px-[18px]">
        <h3 className="mb-[25px]">
          Register
        </h3>

        <div className='flex flex-col gap-4 mb-8'>
          <TextField
            value={email}
            placeholder='Enter Username/Email'
            large
            onInput={(newVal) => setEmail(newVal)}
          />

          <TextField
            value={username}
            placeholder='Create Username'
            large
            onInput={(newVal) => setUsername(newVal)}
          />

          <PasswordField
            value={password}
            placeholder='Create Password'
            large
            onInput={(newVal) => setPassword(newVal)}
          />

          <PasswordField
            value={confirmPassword}
            placeholder='Confirm Password'
            large
            onInput={(newVal) => setConfirmPassword(newVal)}
            onKeyDown={(e: React.KeyboardEvent<HTMLInputElement>) => {
              e.key === 'Enter' && doRegister()
            }}
          />
        </div>

        <RainbowButton
          disabled={!email || !username || !password || !confirmPassword}
          onClick={doRegister}>
          Register
        </RainbowButton>

        <p className="mt-[52px] flex items-center justify-center gap-1 text-[13px] font-medium">
          <span>Have an account?</span>

          <GoldenText className='cursor-pointer' onClick={() => router.replace('/login')}>
            Login here
          </GoldenText>
        </p>

      </div>
    </Scaffold>
  );
}
