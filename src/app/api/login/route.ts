import { NextRequest } from "next/server";
import { cookies } from "next/headers"
import { youapp } from '@/repo'

export async function POST(req: NextRequest) {
  const payload = await req.json();
  const { identify, password } = payload;

  const response = await youapp.login(identify, identify, password)

  if (response.error) {
    return Response.json({
      code: 500,
      message: response.error
    });
  } else {
    const { message, access_token } = response.data || {};
    if (access_token) {
      cookies().set('access_token', access_token);
      return Response.json(
        {
          code: 200,
          data: {
            access_token: access_token,
          },
        }
      );
    } else {
      cookies().delete('access_token');

      if (message === 'User not found') {
        return Response.json({
          code: 404,
          message,
        });
      } else {
        return Response.json({
          code: 401,
          message: 'Invalid credentials',
        });
      }
    }
  }
}