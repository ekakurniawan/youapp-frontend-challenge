import { NextRequest } from "next/server";
import { CONFIG } from "./data";

// export async function GET() {
//   const {refresh_token} = await exchangeCodeForRefreshToken('???')
//   CONFIG.GOOGLE_REFRESH_TOKEN = refresh_token;
//   const {access_token} = await exchangeRefreshToken();
//   CONFIG.GOOGLE_ACCESS_TOKEN = access_token;
//   return Response.json({
//     refresh_token,
//     access_token,
//   });
// }

// function exchangeCodeForRefreshToken(code: string) {
//   return fetch(`https://www.googleapis.com/oauth2/v4/token`, {
//     method: 'POST',
//     headers: {
//       'Content-Type': 'application/x-www-form-urlencoded',
//     },
//     body: new URLSearchParams({
//       grant_type: 'authorization_code',
//       code,
//       client_id: CONFIG.GOOGLE_CLIENT_ID??'',
//       client_secret: CONFIG.GOOGLE_CLIENT_SECRET??'',
//       redirect_uri: CONFIG.GOOGLE_REDIRECT_URI??'',
//     }),
//   }).then(response => response.json());
// }

function exchangeRefreshToken() {
  return fetch(`https://www.googleapis.com/oauth2/v4/token`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    body: new URLSearchParams({
      grant_type: 'refresh_token',
      client_id: CONFIG.GOOGLE_CLIENT_ID??'',
      client_secret: CONFIG.GOOGLE_CLIENT_SECRET??'',
      refresh_token: CONFIG.GOOGLE_REFRESH_TOKEN??'',
    }),
  }).then(response => response.json());;
}

async function uploadToBucket(file: File, filename: string) {
  return new Promise<{error?: any, data?: string}>(async (resolve, reject) => {
    const bucketName = 'eka-storages';
    const destination = 'youapp/';
    const uploadUrl = `https://storage.googleapis.com/upload/storage/v1/b/${bucketName}/o?uploadType=media&name=${destination}${filename}`;
  
    const response = await fetch(uploadUrl, {
      method: 'POST',
      body: file,
      headers: {
        'Content-Type': file.type,
        Authorization: `Bearer ${CONFIG.GOOGLE_ACCESS_TOKEN}`,
      },
    })

    const data = await response.json();
    
    if(data.error && data.error.code === 401) {
      const {access_token} = await exchangeRefreshToken();
      CONFIG.GOOGLE_ACCESS_TOKEN = access_token;
      uploadToBucket(file, filename).then(resolve).catch(reject);
    } else {
      resolve({data: `https://storage.googleapis.com/${data.bucket}/${data.name}`})
    }
  });
}

export async function POST(req: NextRequest) {
  try {
    const formData = await req.formData();
    const files = formData.getAll('files') as File[];
    const fileToStorage = files[0];
    const filename = `${Date.now()}-${fileToStorage.name}`;
    const {error, data} = await uploadToBucket(fileToStorage, filename)
    if(data) {
      return Response.json({ code: 200, data, message: 'Uploaded successfully!'});
    } else {
      return Response.json(error);  
    }
  } catch (error) {
    return Response.json(error);
  }
}