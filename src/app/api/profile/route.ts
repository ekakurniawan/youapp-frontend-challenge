import { NextRequest } from "next/server";
import { cookies } from "next/headers"
import { youapp } from '@/repo'
import { profileMap } from './data'
import { decodeJWT } from "@/composables/useJWT";
import { CONFIG } from "../upload/data";

function exchangeRefreshToken() {
  return fetch(`https://www.googleapis.com/oauth2/v4/token`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    body: new URLSearchParams({
      grant_type: 'refresh_token',
      client_id: CONFIG.GOOGLE_CLIENT_ID ?? '',
      client_secret: CONFIG.GOOGLE_CLIENT_SECRET ?? '',
      refresh_token: CONFIG.GOOGLE_REFRESH_TOKEN ?? '',
    }),
  }).then(response => response.json());;
}

async function uploadToBucket(file: File, filename: string) {
  return new Promise<{ error?: any, data?: string }>(async (resolve, reject) => {
    const bucketName = 'eka-storages';
    const destination = 'youapp/';
    const uploadUrl = `https://storage.googleapis.com/upload/storage/v1/b/${bucketName}/o?uploadType=media&name=${destination}${filename}`;

    const response = await fetch(uploadUrl, {
      method: 'POST',
      body: file,
      headers: {
        'Content-Type': file.type,
        Authorization: `Bearer ${CONFIG.GOOGLE_ACCESS_TOKEN}`,
      },
    })

    const data = await response.json();

    if (data.error && data.error.code === 401) {
      const { access_token } = await exchangeRefreshToken();
      CONFIG.GOOGLE_ACCESS_TOKEN = access_token;
      uploadToBucket(file, filename).then(resolve).catch(reject);
    } else {
      resolve({ data: `https://storage.googleapis.com/${data.bucket}/${data.name}` })
    }
  });
}

export async function GET(req: NextRequest) {
  const access_token = cookies().get('access_token')?.value || '';
  if (!access_token) {
    return Response.json({
      code: 401,
      message: 'Unauthorized'
    })
  }

  const decoded = decodeJWT(access_token);
  let oldProfile = {};
  if (decoded.payload?.email) {
    oldProfile = profileMap.get(decoded.payload?.email)
  }
  const response = await youapp.getProfile(access_token)

  if (response.error) {
    cookies().delete('access_token');
    return Response.json({
      code: 500,
      message: response.error
    });
  } else {
    if (response.data.auth === false) {
      cookies().delete('access_token');
      return Response.json({
        code: 401,
        message: 'Unauthorized'
      });
    } else {
      // if(profileMap.has('')) {
      const { email, username, name, birthday, height, weight, interests } = response.data.data
      return Response.json({
        code: 200,
        data: {
          ...oldProfile,
          email, //
          username, //
          name,
          birthday,
          height,
          weight,
          interests //
        },
      });
    }
  }
}

export async function POST(req: NextRequest) {
  const access_token = cookies().get('access_token')?.value || '';
  if (!access_token) {
    return Response.json({
      code: 401,
      message: 'Unauthorized'
    })
  }

  const decoded = decodeJWT(access_token)

  const formData = await req.formData();
  const { image, gender, ...others } = Object.fromEntries(formData.entries())
  let imageUrl;
  if (image && decoded.payload?.email) {
    const fileToStorage = image;
    const uploaded = await uploadToBucket(fileToStorage as File, `${Date.now()}-${decoded.payload.email}`)
    imageUrl = uploaded.data
  }

  const response = await youapp.createProfile(access_token, {
    name: others.name.toString(),
    birthday: others.birthday.toString(),
    height: Number(others.height),
    weight: Number(others.weight),
    interests: others.interests?.toString().split(',') as string[],
  })

  if (response.error) {
    cookies().delete('access_token');
    return Response.json({
      code: 500,
      message: response.error
    });
  } else {
    const { email, username, name, birthday, height, weight, interests } = response.data.data
    const newData = {
      email, username, name, birthday, height, weight, interests,
      gender: gender,
      image: imageUrl
    }
    profileMap.set(email, newData)
    return Response.json({
      code: 200,
      data: newData,
    });
  }
}

export async function PUT(req: NextRequest) {
  const access_token = cookies().get('access_token')?.value || '';
  if (!access_token) {
    return Response.json({
      code: 401,
      message: 'Unauthorized'
    })
  }

  const decoded = decodeJWT(access_token)

  const formData = await req.formData();
  const { image, gender, ...others } = Object.fromEntries(formData.entries())
  let imageUrl;
  if (image && decoded.payload?.email) {
    const fileToStorage = image;
    const uploaded = await uploadToBucket(fileToStorage as File, `${Date.now()}-${decoded.payload.email}`)
    imageUrl = uploaded.data
  }

  const response = await youapp.updateProfile(access_token, {
    name: others.name.toString(),
    birthday: others.birthday.toString(),
    height: Number(others.height),
    weight: Number(others.weight),
    interests: others.interests?.toString().split(',') as string[],
  })

  if (response.error) {
    cookies().delete('access_token');
    return Response.json({
      code: 500,
      message: response.error
    });
  } else {
    const { email, username, name, birthday, height, weight, interests } = response.data.data
    const newData = {
      email, username, name, birthday, height, weight, interests,
      gender: gender,
      image: imageUrl
    }
    profileMap.set(email, newData)
    return Response.json({
      code: 200,
      data: newData,
    });
  }
}