import { NextRequest } from "next/server";
import { youapp } from '@/repo'

export async function POST(req: NextRequest) {
  const payload = await req.json();
  const { email, username, password } = payload;

  const response = await youapp.register(email, username, password)

  if (response.error) {
    return Response.json({
      code: 500,
      message: response.error
    });
  } else {
    if (response.data.message === 'User has been created successfully') {
      return Response.json({
        code: 200,
        message: response.data.message
      });
    } else if (response.data.message === 'User already exists') {
      return Response.json({
        code: 409,
        message: response.data.message
      });
    } else {
      return Response.json({
        code: 500,
        message: 'Internal server error'
      });
    }
  }
}