import { headers } from "next/headers";
import { NextRequest } from "next/server";

export async function PUT(req: NextRequest) {
  const headersObj = Object.fromEntries(headers().entries())
  const method = req.method;
  const query = Object.fromEntries(req.nextUrl.searchParams.entries());
  let form: any = null;

  let body;
  if(headersObj['content-type'] === 'application/json') {
    body = await req.json();
  } else {
    const formData = await req.formData();
    form = Array.from(formData.entries()).map(([key, value]) => {
      return typeof value === 'object' ? [typeof value, key, {
        name: value.name,
        size: value.size,
        type: value.type,
      }] : [typeof value, key, value]
    });
  }

  return Response.json({
    type: headersObj['content-type'],
    url: req.url,
    method,
    headers: headersObj,
    body,
    form,
    query,
  })
}