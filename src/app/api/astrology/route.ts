import type { Astrology } from "@/models";
import { astrologyData, astrologyFallback } from "./data";

export async function GET() {
  if (astrologyData.length === 0) {
    const raw = await fetchAstrology()
    const parsed = generateAstrologyObjects(raw);
    astrologyData.push(...parsed);
  }
  return Response.json(astrologyData);
}

async function fetchAstrology() {
  const API_URL = 'https://sheets.googleapis.com/v4/spreadsheets'
  const SPREADSHEET_ID = '1Oahej8yuEHfDsQI-AwycEpQ0CnjkMsxOMg2ywMKnjsg'
  const API_KEY = 'AIzaSyArAU1YJwTAW26qTqvMi1lTYhEtUoSrYcg'
  const RANGES = 'Horoscope'
  return await fetch(`${API_URL}/${SPREADSHEET_ID}/values/${RANGES}?key=${API_KEY}`)
    .then(res => res.json())
    .then(res =>
      res.values
        .filter((item: string[]) => item.length)
        .flat() ?? astrologyFallback)
    .catch(err => {
      console.warn(err)
      return []
    })
}

function generateAstrologyObjects(data: string[]): Astrology[] {
  return data.reduce<Astrology[]>((acc, current) => {
    const parts = current.match(/(♈|♉|♊|♋|♌|♍|♎|♏|♐|♑|♒|♓)\s*(\w+)\s*\((\w+)\):\s*(\w+\s*\d+–\w+\s*\d+)/);
    if (!parts) return acc;

    const [_, symbol, name, abbr, dateRange] = parts;
    const [
      [startMonth, startDate],
      [endMonth, endDate]
    ] = dateRange
      .split('–')
      .map(date => date.trim().split(' '));

    return [...acc, {
      symbol,
      name,
      abbr, //abbreviation
      range: [
        [parseInt(startDate), getMonth(startMonth)],
        [parseInt(endDate), getMonth(endMonth)]
      ]
    }];
  }, []);
}

function getMonth(monthStr: string) {
  const date = new Date(`${monthStr} 1, 2024`) // added 2024 to avoid leap year issues
  return date.getMonth() + 1 // getMonth() returns 0-11;
}