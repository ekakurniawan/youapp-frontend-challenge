'use client';

import Image from 'next/image'
import RainbowButton from '@/components/Common/RainbowButton';
import { useRouter } from 'next/navigation';
import { useToken } from '@/composables/useToken';

export default function Home() {
  const router = useRouter()

  const [access_token, setAccessToken] = useToken();

  function doLogout() {
    localStorage.removeItem('access_token')
    setAccessToken(undefined)
  }

  return (
    <div className="h-full flex flex-col justify-center px-4 gap-10">
      <div className="flex justify-center px-12">
        <Image alt="Logo" src="https://youapp.ai/_next/image?url=%2Fstatic%2Fimages%2Fyouapp-full.png&w=384&q=100" priority width={384} height={92} />
      </div>
      <div className="flex flex-col items-center justify-center">
        <p className="text-base font-bold tracking-widest">Technical Challenge</p>
        <p className="text-base font-bold tracking-widest">Frontend</p>
      </div>
      {
        !!access_token ? (
          <div className='flex flex-col gap-4'>
            <RainbowButton onClick={() => router.push('/profile')}>
              Profile
            </RainbowButton>

            <RainbowButton onClick={doLogout}>
              Logout
            </RainbowButton>
          </div>
        ) : (
          <RainbowButton onClick={() => router.push('/login')}>
            Begin
          </RainbowButton>
        )
      }

    </div>
  );
}



