import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "./globals.css";
import { AstrologyProvider } from "@/components/AstrologyProvider";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Youapp - Frontend Challenge",
  description: "Technical Challenge Frontend",
  generator: "Next.js",
  manifest: "/static/manifest.json",
  keywords: ["nextjs", "nextjs13", "next13", "pwa", "next-pwa"],
  themeColor: [{ media: "(prefers-color-scheme: dark)", color: "#fff" }],
  authors: [
    { name: "Eka Kurniawan" },
    {
      name: "Eka Kurniawan",
      url: "https://gitlab.com/ekakurniawan/youapp-frontend-challenge",
    },
  ],
  viewport:
    "minimum-scale=1, initial-scale=1, width=device-width, shrink-to-fit=no, viewport-fit=cover",
  icons: [
    { rel: "apple-touch-icon", url: "/static/favicon/android-icon-96x96.png" },
    { rel: "icon", url: "/static/favicon/android-icon-96x96.png" },
  ],
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <head>
        <meta name="viewport" content="width=device-width, initial-scale=1"></meta>
        <meta name="theme-color" content="#09141A"></meta>
        <meta name="apple-mobile-web-app-capable" content="yes"></meta>
        <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"></meta>
      </head>
      <body className={inter.className}>
        <main className={[
          "fixed inset-0",
          'bg-[#1E1E1E]',
          'flex flex-col items-center justify-center'
        ].join(' ')}>
          <div className={[
            'relative',
            'transition-all duration-300',
            'my-0 md:my-10',
            "w-full h-full max-w-full md:max-w-sm max-h-full sm:max-h-[768px] mx-auto rounded-none sm:rounded-3xl overflow-hidden"
          ].join(' ')}
            style={{
              background: 'radial-gradient(124.23% 171.99% at 100% -3.39%, #1F4247 0%, #0D1D23 56.18%, #09141A 100%)'
            }}
          >
            <AstrologyProvider>
              {children}
            </AstrologyProvider>
          </div>
        </main>
      </body>
    </html >
  );
}
