'use client';

import { useEffect, useState } from "react";

declare global {
  interface Window {
    cookieStore: any;
  }
}

export function useToken(tokenName: string = 'access_token') {
  if (typeof window === 'undefined') {
    return [null, () => { }] as const;
  }

  const [access_token, setAccessTokenState] = useState<string | undefined>();

  useEffect(() => {
    const handleStorageChange = () => {
      window.cookieStore.get(tokenName).then((c: any) => {
        setAccessTokenState(c.value)
      })
    };

    // window.cookieStore.onchange = handleStorageChange
    window.cookieStore.addEventListener('onchange', handleStorageChange)

    window.cookieStore.get(tokenName).then((c: any) => {
      setAccessTokenState(c?.value)
    })

    return () => {
      window.cookieStore.onchange = null
      window.cookieStore.removeEventListener('onchange', handleStorageChange)
    };
  }, [tokenName]);

  const setAccessToken = (newToken: string | undefined) => {
    setAccessTokenState(newToken);
    if (newToken === undefined) {
      window.cookieStore.delete(tokenName)
    } else {
      window.cookieStore.set(tokenName, newToken)
    };
  };

  // useEffect(() => {
  //   const handleStorageChange = () => {
  //     setAccessTokenState(localStorage.getItem(tokenName));
  //   };

  //   window.addEventListener('storage', handleStorageChange);
  //   setAccessTokenState(localStorage.getItem(tokenName));

  //   return () => {
  //     window.removeEventListener('storage', handleStorageChange);
  //   };
  // }, [tokenName]);

  // const setAccessToken = (newToken: string | null) => {
  //   setAccessTokenState(newToken);
  //   if (newToken === null) {
  //     localStorage.removeItem(tokenName)
  //   } else {
  //     localStorage.setItem(tokenName, newToken);
  //   };
  // };

  return [access_token, setAccessToken] as const;
}
