import type { Astrology } from "@/models";
import { createContext } from "react";

export async function useAstrology() {
  const response = await fetch('/api/astrology').then(res => res.json())
  return response
}

export const AstrologyContext = createContext<{
  astrology: Astrology[],
  getAstrology: (date: number[]) => (Astrology & { shio: string }) | null
}>({
  astrology: [],
  getAstrology: () => null
});


