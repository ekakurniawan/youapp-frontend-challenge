export function decodeJWT(token: string) {
  const [base64Header, base64Url, signature] = token.split('.');
  const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
  const jsonPayload = decodeURIComponent(
    atob(base64)
      .split('')
      .map((c) => {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
      })
      .join('')
  );

  return {
    header: JSON.parse(atob(base64Header)),
    payload: JSON.parse(jsonPayload),
    signature: signature
  };
}

export function encodeJWT(payload: any) {
  const jsonStr = JSON.stringify(payload);
  const jsonPayload = btoa(jsonStr);
  return jsonPayload;
}

// export const useJWT = () => {
//   return {
//     decodeJWT,
//     encodeJWT
//   }
// }

// const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY1YjdjYzA0MjM0YzQ2ZTk3MmQyNDQ1MiIsInVzZXJuYW1lIjoiYm90MDEiLCJlbWFpbCI6ImJvdDAxQGdtYWlsLmNvbSIsImlhdCI6MTcwODY1OTU0OCwiZXhwIjoxNzA4NjYzMTQ4fQ.GpdGnafLafJt1mrVJtkfdU-HgL1WwFbSyffbPPIje1Y"
// console.log(decodeJWT(token))

// const current = new Date()
// current.setDate(current.getDate() + 1);
// // to unix timestamp
// console.log(Math.round(current.getTime() / 1000))