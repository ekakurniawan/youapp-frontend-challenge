import { RectShape, TextBlock, TextRow } from "@/components/Placeholder";

const BannerPlaceholder = (
  <div className='relative aspect-video rounded-2xl overflow-hidden'>
    <RectShape color='#3a3a3a' className="absolute inset-0" />

    <div className="absolute left-0 bottom-0 right-0 p-4">
      <TextBlock rows={2} color='gray' lineSpacing={16} widths={[60, 40]} />

      <TextRow maxHeight={40} color="gray" />
    </div>
  </div>
);

export default BannerPlaceholder;