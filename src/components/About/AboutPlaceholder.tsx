import { RectShape, TextBlock } from "@/components/Placeholder";

const AboutPlaceholder = (
  <div className='relative aspect-video rounded-2xl overflow-hidden'>
    <RectShape color='#3a3a3a' className="absolute inset-0" />

    <div className="absolute inset-0 p-8">
      <TextBlock rows={5} color='gray' lineSpacing={16} widths={[30, 0, 80, 40, 40]} />
    </div>
  </div>
);

export default AboutPlaceholder;