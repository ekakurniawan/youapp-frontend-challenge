
import { AstrologyContext } from '@/composables/useAstrology';
import { Profile } from '@/models'
import { useContext, useMemo } from 'react';

type DisplayProps = {
  profile: Profile;
}

const parseDate = (date: string) => {
  if (!date) {
    return null;
  } else {
    const [day, month, year] = date.split(' ');
    return [parseInt(day), parseInt(month), parseInt(year)];
  }
}

const Display = ({ profile }: DisplayProps) => {
  const astrologyContext = useContext(AstrologyContext)

  const astrology = useMemo(() => {
    if (!profile.birthday) return undefined;

    const parsedDate = parseDate(profile.birthday);
    if(!parsedDate) {
      return {
        name: '--',
        shio: '--'
      }
    } else {
      return astrologyContext.getAstrology(parsedDate);
    }
  }, [profile.birthday, astrologyContext])

  const age = useMemo(() => {
    const [day, month, year] = profile?.birthday?.split(' ') ?? [];
    if (!day || !month || !year) return null;

    const birthday = new Date(`${month} ${day}, ${year}`);
    const diff = Date.now() - birthday.getTime();
    const ageDate = new Date(diff);
    return Math.abs(ageDate.getUTCFullYear() - 1970);
  }, [profile.birthday]);

  return (
    <div className="my-4 flex flex-col gap-3">
      {
        [
          {
            label: 'Birthday:',
            value: profile.birthday ? `${profile.birthday.split(' ').join(' / ')} (Age ${age})` : 'N/A'
          },
          {
            label: 'Horoscope:',
            value: astrology?.name ?? 'N/A'
          },
          {
            label: 'Zodiac:',
            value: astrology?.shio ?? 'N/A'
          },
          {
            label: 'Height:',
            value: profile.height ? `${profile.height} cm` : 'N/A'
          },
          {
            label: 'Weight:',
            value: profile.weight ? `${profile.weight} kg` : 'N/A'
          }
        ].map((item, index) => (
          <div key={index} className="flex items-center gap-1 -mr-2">
            <span className="form-label">{item.label}</span>
            <span className="text-[13px] font-medium">{item.value}</span>
          </div>
        ))
      }

    </div>
  )
}

export default Display