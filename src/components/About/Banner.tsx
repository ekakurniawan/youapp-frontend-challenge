import Image from "next/image";
import { Stack } from "@/components/Panes";
import { useContext, useMemo } from "react";
import { Profile } from '@/models'
import { AstrologyContext } from "@/composables/useAstrology";

type BannerProps = {
  profile: Profile;
}

const parseDate = (date: string) => {
  const [day, month, year] = date.split(' ');
  return [parseInt(day), parseInt(month), parseInt(year)];
}

const Banner = ({ profile }: BannerProps) => {
  const astrologyContext = useContext(AstrologyContext)

  const astrology = useMemo(() => {
    if (!profile.birthday) return undefined;

    const parsedDate = parseDate(profile.birthday);
    return astrologyContext.getAstrology(parsedDate);
  }, [profile.birthday, astrologyContext])

  const username = useMemo(() => {
    return profile?.username ? `@${profile.username}` : 'N/A'
  }, [profile.username]);

  const age = useMemo(() => {
    const [day, month, year] = profile?.birthday?.split(' ') ?? [];
    if (!day || !month || !year) return null;

    const birthday = new Date(`${month} ${day}, ${year}`);
    const diff = Date.now() - birthday.getTime();
    const ageDate = new Date(diff);
    return Math.abs(ageDate.getUTCFullYear() - 1970);
  }, [profile.birthday]);

  const gender = useMemo(() => {
    return profile.gender === '0' ? 'Female' : profile.gender === '1' ? 'Male' : null
  }, [profile.gender]);

  const image = useMemo(() => {
    return profile.image ? profile.image : null
  }, [profile.image]);

  return (
    <Stack className="aspect-video bg-[#162329] rounded-2xl">
      <div className="absolute inset-0">
        {
          !!image && (
            <>
              <Image src={image} alt="image" width={100} height={100} priority className="w-full h-full object-cover" />
              <div className="absolute inset-0" style={{
                background: 'linear-gradient(180deg, rgba(0, 0, 0, 0.76) 0%, rgba(0, 0, 0, 0.00) 45.83%, #000 100%)'
              }}></div>
            </>
          )
        }
      </div>

      <div className="absolute bottom-0 ml-[13px] mb-[17px] flex flex-col">
        <span className="text-base font-bold tracking-tight" style={{
          textShadow: '0 1px 2px rgb(0 0 0)'
        }}>
          {username}, {age}
        </span>

        {
          !!gender && (
            <span className="mt-1 text-[13px] font-medium" style={{
              textShadow: '0 1px 2px rgb(0 0 0)'
            }}>
              {gender}
            </span>
          )
        }

        <div className="mt-2 flex gap-4">
          {
            astrology?.name && (<div className="rounded-[100px] bg-white/[6%] [backdrop-filter:blur(25px)] px-4 py-2 flex items-center gap-2">
              <Image
                alt={astrology.name}
                src={`/static/images/horoscope/${astrology.name.toLowerCase()}.svg`}
                width={20}
                height={20}
                className="w-[20px] h-[20px]"
                style={{ color: 'transparent' }}
              />
              <span className=""> {astrology.name} </span>
            </div>)
          }

          {
            astrology?.shio && (<div className="rounded-[100px] bg-white/[6%] [backdrop-filter:blur(25px)] px-4 py-2 flex items-center gap-2">
              <Image
                alt={astrology.shio}
                src={`/static/images/zodiac/${astrology.shio.toLowerCase()}.svg`}
                width={20}
                height={20}
                className="w-[20px] h-[20px]"
                style={{ color: 'transparent' }}
              />
              <span className=""> {astrology.shio} </span>
            </div>)
          }
        </div>
      </div>
    </Stack>
  )
}

export default Banner