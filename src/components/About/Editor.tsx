'use client';

import { TextField, SelectField, DateField, UnitField } from '@/components/Inputs'
import { useCallback, useContext, useEffect, useMemo, useState } from "react";
import { IconImageAdd } from '../Icons';
import BrowseFile from '../Common/BrowseFile';
import { AstrologyContext } from '@/composables/useAstrology';

type EditorProps = {
  profile?: Record<string, any>;
  onChange?: (value: Record<string, any>) => void;
}

const parseDate = (date: string) => {
  if (!date) {
    return null;
  } else {
    const [day, month, year] = date.split(' ');
    return [parseInt(day), parseInt(month), parseInt(year)];
  }
}

const Editor = ({ profile = {}, onChange }: EditorProps) => {
  const astrologyContext = useContext(AstrologyContext)
  const [payload, setPayload] = useState<Record<string, any>>({
    name: 'John Doe',
    gender: '1',
    birthday: '28 08 1995',
    horoscope: 'Virgo',
    zodiac: 'Pig',
    height: 175,
    weight: 69,
    image: null,
    ...profile,
  })

  useEffect(() => {
    setPayload((prev) => ({ ...prev, ...profile }));
  }, [profile])


  useEffect(() => {
    onChange?.(payload)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [payload])

  const zodiac = useMemo(() => {
    const parsedDate = parseDate(payload.birthday);
    if (!parsedDate) {
      return {
        name: '--',
        shio: '--'
      }
    } else {
      return astrologyContext.getAstrology(parsedDate);
    }
  }, [payload.birthday, astrologyContext])

  useEffect(() => {
    handleInput('horoscope', zodiac?.name);
    handleInput('zodiac', zodiac?.shio);
    return () => void 0;
  }, [zodiac])

  const handleInput = (key: string, value: any) => {
    setPayload((prev) => ({ ...prev, [key]: value }));
  };

  const fields = useMemo(() => [
    {
      key: 'name',
      label: 'Display Name:',
      placeholder: 'Enter name',
    },
    {
      key: 'gender',
      type: 'select',
      label: 'Gender:',
      placeholder: 'Select Gender',
      options: [
        {
          label: 'Male',
          value: '1',
        },
        {
          label: 'Female',
          value: '0',
        }
      ]
    },
    {
      key: 'birthday',
      type: 'date',
      label: 'Birthday:',
      placeholder: 'DD MM YYYY',
    },
    {
      key: 'horoscope',
      label: 'Horoscope:',
      placeholder: '--',
      disabled: true
    },
    {
      key: 'zodiac',
      label: 'Zodiac:',
      placeholder: '--',
      disabled: true
    },
    {
      key: 'height',
      type: 'unit',
      label: 'Height:',
      placeholder: 'Add height',
      units: [
        {
          label: 'cm',
          formula: (val: number) => Number.parseFloat((val * 2.54).toFixed(2)),
          primary: true
        },
        {
          label: 'inch',
          formula: (val: number) => Number.parseFloat((val * 0.3937).toFixed(2)),
        }
      ]
    },
    {
      key: 'weight',
      type: 'unit',
      label: 'Weight:',
      placeholder: 'Add weight',
      units: [
        {
          label: 'kg',
          formula: (val: number) => val
        }
      ]
    },
  ], [])

  const [fileTemp, setFileTemp] = useState<File | null>(null)

  function onFileChange(file: File | null) {
    setFileTemp(file)
  }

  useEffect(() => {
    if (fileTemp) {
      handleInput('image', URL.createObjectURL(fileTemp))
    }
  }, [fileTemp])

  return (
    <>
      <div className="mb-[29px] mt-6">
        <BrowseFile onChange={onFileChange}>
          <button
            aria-label="Add Image"
            className='flex items-center gap-[15px]'>
            {
              (fileTemp === null && !payload.image) ? (
                <div className="w-[57px] h-[57px]">
                  <IconImageAdd />
                </div>
              ) : (
                <img
                  alt="temp-image"
                  src={payload.image}
                  width={57}
                  height={57}
                  className="w-[57px] h-[57px] rounded-[17px] object-cover"
                />
              )
            }

            <span className="text-xs font-medium">
              Add image
            </span>
          </button>
        </BrowseFile>
      </div>

      <div className="flex flex-col gap-3 mb-6">
        {
          fields.map((item, index) => {
            const sameFields = {
              value: payload[item.key],
              placeholder: item.placeholder,
              onInput: (newVal: unknown) => handleInput(item.key, newVal)
            }
            // TODO: define type of dynamic component
            const componentSet: any = {
              'select': [SelectField, { options: item.options, }],
              'date': [DateField, {}],
              'unit': [UnitField, { units: item.units, }],
              'text': [TextField, { disabled: item.disabled, outlined: true, alignRight: true }],
            }
            const [dynamicComponent, args] = componentSet[item.type ?? 'text'] || [() => <div />, {}]

            return (
              <div key={index} className="flex items-center justify-between -mr-2">
                <span className="form-label flex-1">
                  {item.label}
                </span>

                <div className='w-3/5'>
                  {dynamicComponent({ ...sameFields, ...args })}
                </div>
              </div>
            )
          })
        }
      </div>
    </>
  )
}

export default Editor