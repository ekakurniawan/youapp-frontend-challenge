'use client';
import type { Astrology } from "@/models"
import { AstrologyContext, useAstrology } from "@/composables/useAstrology";
import { useEffect, useState } from "react";

function getShio(dateOfBirth: string) {
  let birthDate = new Date(dateOfBirth);
  let year = birthDate.getFullYear();
  let zodiacIndex = (year - 1900) % 12;
  return ["Rat", "Ox", "Tiger", "Rabbit", "Dragon", "Snake", "Horse", "Goat", "Monkey", "Rooster", "Dog", "Pig"][zodiacIndex];
}

export const AstrologyProvider = ({ children }: { children: React.ReactNode }) => {
  const [astrology, setAstrology] = useState<Astrology[]>([]);
  useEffect(() => {
    useAstrology().then((zodiac) => {
      setAstrology(zodiac);
    });
  }, [])

  const getAstrology = (date: number[]) => {
    const [day, month, year] = date;
    const found = astrology.find(item => {
      const [sDay, sMonth] = item.range[0];
      const [eDay, eMonth] = item.range[1];
      return (sMonth === month && sDay <= day) || (eMonth === month && eDay >= day);
    });

    return found ? {
      ...found,
      shio: getShio(`${year}-${month}-${day}`)
    } : null;
  }

  return (
    <AstrologyContext.Provider value={{ astrology, getAstrology }}>
      {children}
    </AstrologyContext.Provider>
  );
};