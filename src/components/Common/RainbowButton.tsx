import { IconLoading } from "../Icons";

type RainbowButtonProps = {
  children?: React.ReactNode;
  disabled?: boolean;
  isLoading?: boolean;
  onClick?: () => void;
}

const RainbowButton = ({ children, isLoading = false, disabled = false, onClick }: RainbowButtonProps) => {
  return (
    <button aria-label="Action" disabled={disabled} className={[
      "h-12 shrink-0 rounded-lg flex items-center justify-center cursor-pointer",
      'disabled:opacity-50 disabled:cursor-not-allowed'
    ].join(' ')}
      style={{
        background: 'linear-gradient(108deg, #62CDCB 24.88%, #4599DB 78.49%)'
      }}
      onClick={onClick}
    >
      {
        isLoading ? (
          <IconLoading />
        ) : (
          <span className="text-base font-bold">
            {children}
          </span>
        )
      }
    </button>
  )
}

export default RainbowButton;