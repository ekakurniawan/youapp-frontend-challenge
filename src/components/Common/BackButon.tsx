import { IconChevronLeft } from '@/components/Icons'

type BackButtonProps = {
  onClick?: () => void;
};

const BackButton = ({ onClick }: BackButtonProps) => {
  return (
    <button
      aria-label="Back"
      className="flex items-center gap-2.5"
      onClick={onClick}>
      <IconChevronLeft />

      <span className="text-sm font-bold">Back</span>
    </button>
  )
}

export default BackButton;