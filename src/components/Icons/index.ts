import ChevronLeft from "./ChevronLeft";
import HorizontalDot from "./HorizontalDot";
import ImageAdd from "./ImageAdd";
import Edit from "./Edit";
import Close from "./Close";
import Invisible from "./Invisible";
import Visible from "./Visible";
import Loading from "./Loading";

export {
  ChevronLeft as IconChevronLeft,
  HorizontalDot as IconHorizontalDot,
  ImageAdd as IconImageAdd,
  Edit as IconEdit,
  Close as IconClose,
  Invisible as IconInvisible,
  Visible as IconVisible,
  Loading as IconLoading
}