'use client';

import { useEffect, useMemo, useState } from "react";

type TextFieldProps = {
  value?: string
  placeholder?: string
  disabled?: boolean
  onInput?: (value: string) => void
  outlined?: boolean,
  alignRight?: boolean
  grow?: boolean
  large?: boolean
}

const TextField = ({
  value = '',
  placeholder = '',
  disabled = false,
  outlined = false,
  alignRight = false,
  grow = true,
  large = false,
  onInput
}: TextFieldProps) => {
  const [internalValue, setInternalValue] = useState(value);

  useEffect(() => {
    setInternalValue(value)
  }, [value])

  const handleInput = (event: React.ChangeEvent<HTMLInputElement>) => {
    setInternalValue(event.target.value);
    onInput?.(event.target.value);
  };

  return (
    <div className={[
      grow ? 'grow' : '',
      'bg-white/[6%] flex items-center border',
      large ? 'h-[51px] px-[18px] py-3 rounded-[9px]' : 'h-9 px-5 rounded-lg',
      outlined ? 'border-white/[22%]' : 'border-transparent',
      'focus-within:border-[#62CDCB] focus-within:border-opacity-50'
    ].join(' ')}>
      <input type="text"
        value={internalValue}
        placeholder={placeholder}
        disabled={disabled}
        className={[
          'w-[-webkit-fill-available] bg-transparent font-medium outline-none',
          large ? 'text-base' : 'text-[13px]',
          'disabled:text-white/[30%]',
          alignRight ? 'text-right placeholder:text-right' : '',
        ].join(' ')}
        onInput={handleInput}
      />
    </div>
  )
}
export default TextField