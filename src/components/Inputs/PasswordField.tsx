'use client';

import { useEffect, useMemo, useState } from "react";
import { IconInvisible, IconVisible } from "../Icons";

type PasswordFieldProps = {
  value?: string
  placeholder?: string
  disabled?: boolean
  onInput?: (value: string) => void
  onKeyDown?: (event: React.KeyboardEvent<HTMLInputElement>) => void
  outlined?: boolean,
  alignRight?: boolean
  grow?: boolean
  large?: boolean
}

const PasswordField = ({
  value = '',
  placeholder = '',
  disabled = false,
  outlined = false,
  alignRight = false,
  grow = true,
  large = false,
  onInput,
  onKeyDown
}: PasswordFieldProps) => {
  const [internalValue, setInternalValue] = useState(value);
  const [showPassword, setShowPassword] = useState(false);

  useEffect(() => {
    setInternalValue(value)
  }, [value])

  const handleInput = (event: React.ChangeEvent<HTMLInputElement>) => {
    setInternalValue(event.target.value);
    onInput?.(event.target.value);
  };

  return (
    <div className={[
      grow ? 'grow' : '',
      'bg-white/[6%] flex items-center border',
      large ? 'px-[18px] py-3 rounded-[9px]' : 'px-5 rounded-lg',
      outlined ? 'border-white/[22%]' : 'border-transparent',
      'focus-within:border-[#62CDCB] focus-within:border-opacity-50'
    ].join(' ')}>
      <input type="text"
        value={internalValue}
        placeholder={placeholder}
        disabled={disabled}
        className={[
          'w-[-webkit-fill-available] bg-transparent font-medium outline-none',
          large ? 'text-base' : 'text-[13px]',
          'disabled:text-white/[30%]',
          alignRight ? 'text-right placeholder:text-right' : '',
          !showPassword ? '[-webkit-text-security:disc]' : '',
        ].join(' ')}
        onInput={handleInput}
        onKeyDown={onKeyDown}
      />

      <div
        className="cursor-pointer select-none"
        onClick={() => setShowPassword(prev => !prev)}>
        {
          showPassword ? <IconVisible /> : <IconInvisible />
        }
      </div>
    </div>
  )
}
export default PasswordField