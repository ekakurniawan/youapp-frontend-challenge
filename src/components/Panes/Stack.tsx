type StackProps = {
  className?: string;
  children?: React.ReactNode;
}

const Stack = ({ className, children }: StackProps) => {
  return (
    <div className={["relative overflow-hidden", className].join(' ')}>
      {children}
    </div>
  );
}

export default Stack;