type ScaffoldProps = {
  appBar?: React.ReactNode
  children?: React.ReactNode
  background?: number
}

const Scaffold = ({ appBar, children, background = 0 }: ScaffoldProps) => {
  return (
    <div className="w-full h-full flex flex-col transition-all duration-300"
      style={{
        background: background === 0
          ? `#09141A`
          : `radial-gradient(124.23% 171.99% at 100% -3.39%, #1F4247 0%, #0D1D23 56.18%, #09141A 100%)`
      }}
    >
      {appBar}
      <div className="relative flex-1 pt-3 pb-6 flex flex-col overflow-auto">
        {children}
      </div>
    </div>
  )
}

export default Scaffold;